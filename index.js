// console.log("Hello World");

// [SECTION] - JavaScript Synchronous vs Asynchronous

console.log("Hello World!");
// conosle.log("Hello Again!");
console.log("Goodbye");

console.log("Hello World!");
/*for(let i = 0; i <= 1500; i++){
	console.log(i);
}*/
console.log("Hello Again!");


// Section - Geeting all post
// fetch() - fetch request

/*
	syntax -- //fetch()
	console.log(fetch('https://jsonplaceholder.typicode.com/posts'))
*/

fetch("https://jsonplaceholder.typicode.com/posts")
// We use "json" method from the response object to convert data into JSON format
.then((response)=> response.json())
.then((json) => console.log(json));

// async and await

async function fetchData(){
	let result = await fetch("https://jsonplaceholder.typicode.com/posts");
	console.log(result);
	console.log(typeof result);
	console.log(result.body);

	let json = await result.json();
	console.log(json);
}

fetchData();

// Section - Getting a specific post

fetch("https://jsonplaceholder.typicode.com/posts/1")
.then((response)=> response.json())
.then((json)=> console.log(json));


// Section - Creating a post

/*

	SYNTAX:

	fetch("URL", options)
	.then((argument) => {})
	.then((argument) => {})
*/

fetch("https://jsonplaceholder.typicode.com/posts/", {
	method : "POST",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		title: "New Post",
		body: "Hello World!",
		userId: 1
	})
})
.then((response)=> response.json())
.then((json)=> console.log(json));

// section updating a post
// PUT Method

fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "PUT",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		id: 1,
		title: "Updated Post",
		body: "Hello Again!",
		userId: 1
	})
})
.then((response)=> response.json())
.then((json)=> console.log(json));

// Section - Patch Method

fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "PATCH",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		id: 1,
		title: "Corrected Post"
	})
})
.then((response)=> response.json())
.then((json)=> console.log(json));

// Section - Deleting a post

fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "DELETE"
})



// Section - Filtering Post

/*
	Syntax
	Individual Parameter
	// 'url?parameterName=value'
	Multiple
	//url?paramA=valueA&paramB=valueB
*/

fetch("https://jsonplaceholder.typicode.com/posts?userId=2")
.then((response)=> response.json())
.then((json)=> console.log(json));


// Section - Retrieving comments on a specific post

fetch("https://jsonplaceholder.typicode.com/posts/1/comments")
.then((response)=> response.json())
.then((json)=> console.log(json));

console.log(fetch("https://jsonplaceholder.typicode.com/posts"));